# Quality control report for GEM

This is platform generates QC REPORT reports for **GE1/1**, **GE2/1** and **ME0**. In addition, it saves the reports in a database for later queries.  

[https://qc-report-git-qc-gem.app.cern.ch/](https://qc-report-git-qc-gem.app.cern.ch/)


## Description of files in this repository:

1. `gem_qc_report/` contains the modules of the package:

   * `manage.py` is the main module. This runs the project and allows you to have a local server.

   * `requirements.txt` Contains all the libraries to be installed for the operation of the project.

   * `.gitlab-ci.yml ` Is the pipelane test for gilab 
     

## Installation

**NOTE:** *This project was made in* ***Python 3.8.0***, this is the version that must be used:

1. Install ***Python 3.8.0***

2. Clone the repository
    ```

    git clone https://gitlab.cern.ch/udea/qc_report.git
    cd qc_report

    ```

3. Create the virtual environment, you can use the following commands:

    ```
    python3.8 -m venv name_venv
    ```
   **name_venv** is the name virtual inveroment, it is recommended to use **venv**

4. Activate virtual environment

    ### Ubuntu
   ```
   source name_venv/bin/activate
   ```

   ### MacOS
   ```
   source name_venv/bin/activate
   ```

   ### Windows
   ```
   .\name_venv\Scripts\activate
   ```
5. Install libraries
   ```
   pip install -r requirements.txt
   ```

## Launch local server (use tool)

To launch the server you must follow the steps below:

1. Launch local server 

```
python manage.py runserver
```
You must have active the virtual environment from the previous step

2. **When you launch the server it will give you a url, you can click it and it will open the tool (Image of what it looks like)**

![alt text](linkurl.png)

3. **Using the tool**

![alt text](imagepage.png)


## Contributing

This project is in progress and it requires some improvments. Therefore, if you have any suggestion that would make this better, please fork the repository and create a pull request. You can also simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/improvments`)
3. Commit your Changes (`git commit -m 'Adding some improvment`)
4. Push to the Branch (`git push origin feature/improvments`)
5. Open a Pull Request

## Contact

* [A. Alexis Ruales B](https://www.linkedin.com/in/anderson-alexis-ruales-b27638199/?originalSubdomain=co) - arualesb.1@cern.ch

    