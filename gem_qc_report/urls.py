from django.urls import path
from .views import Home

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', Home().login, name='home'),
    path('upload/', Home().upload_images, name='upload_images'),
    path('comentarios/', Home().handle_comments, name='handle_comments'),


    path('', Home().clean, name='clear'),

]

