from django.apps import AppConfig


class GemQcReportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gem_qc_report'
