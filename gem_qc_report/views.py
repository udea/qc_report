from django.shortcuts import render,redirect
from django.http import JsonResponse
import os
from django.core.files.storage import FileSystemStorage
import tempfile
from django.core.files.storage import default_storage
from .forms import ImageForm
from .models import Image
import time
from io import BytesIO


import glob

from django.conf import settings

from django.http import HttpResponse
import json

#PDF
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

import PIL as pli
from datetime import datetime
from reportlab.platypus import Paragraph
from reportlab.lib.units import inch

import reportlab.platypus as imr#import Image, Paragraph

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
print("BASE_DIR",BASE_DIR)


class Home:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.context = {'image_urlsvi': [], 'image_urlsmi': []}

        


    
    def login(self,request):
        if len(self.context["image_urlsmi"])>0 and  len(self.context["image_urlsvi"])>0:
            self.context = {'image_urlsvi': [], 'image_urlsmi': []}

        if 'form_typevi' in request.POST and request.POST['form_typevi'] == 'upload_imagesvi':
            self.context['image_urlsvi']=[]
            files = request.FILES.getlist('imagesvi')
            for file in files:
                img_instance = Image.objects.create(image=file)
                self.context['image_urlsvi'].append(img_instance.image.url)

        elif 'form_typemi' in request.POST and request.POST['form_typemi'] == 'upload_imagesmi':

            self.context['image_urlsmi']=[]
            filesmi = request.FILES.getlist('imagesmi')
            for file in filesmi:
                img_instance = Image.objects.create(image=file)
                self.context['image_urlsmi'].append(img_instance.image.url)
        
        #print(self.context)
        
        return render(request, 'home.html', self.context)



    def upload_images(self,request):
        context={}
        if request.method == 'POST':
            print("HOLA ALEXIS")

            files = request.FILES.getlist('imagesvi')  # Asegúrate de que esto coincida con el nombre de tu campo input
            image_paths = []
            #print(files)
            for file in files:
                img_instance = Image.objects.create(image=file)
                relative_path = img_instance.image.url
                image_paths.append(relative_path)
            
            filesmi = request.FILES.getlist('imagesmi')

            image_pathsmi = []
            #print(files)
            for file in filesmi:
                img_instance = Image.objects.create(image=file)
                relative_path = img_instance.image.url
                image_pathsmi.append(relative_path)

            print("imagesmi = request.FILES.getlist('imagesmi')",image_pathsmi)
            
  
            
            context = {'image_urlsvi': image_paths,'image_urlsmi': image_pathsmi}

            #print(context)

        return render(request, 'upload.html',context)




    def handle_comments(self,request):
        if request.method == 'POST':
            self.comments_data = dict(request.POST.lists())
            #print(self.comments_data)


            boardName=self.comments_data['boardName'][0]
            gemName=self.comments_data['gemName'][0]

            geminformation=self.comments_data['information'][0]

            operatorvi=self.comments_data['operatorvi'][0]
            datavi=self.comments_data['datevi'][0]

            images_ulr_vi=json.loads(self.comments_data['imageUrlsvi'][0])
            comments_vi=json.loads(self.comments_data['commentsvi'][0])

            operatormi=self.comments_data['operatormi'][0]
            datami=self.comments_data['datemi'][0]

            images_ulr_mi=json.loads(self.comments_data['imageUrlsmi'][0])

            #print("images_ulr_mi",images_ulr_mi)
            


            pdf_buffer = self.generate_pdf_with_image(boardName,gemName,geminformation,operatorvi,datavi,
                                                      images_ulr_vi,comments_vi,operatormi,datami,images_ulr_mi)
            
            # Asegúrate de que el buffer esté en el inicio del stream antes de leerlo
            pdf_buffer.seek(0)

            response = HttpResponse(pdf_buffer, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="Report_test.pdf"'
            return response

        # Manejo para métodos no POST, si es necesario
        return HttpResponse("Method Not Allowed", status=405)

    
    def generate_pdf_with_image(self,boardName,gemName,geminformation,operatorvi,datavi,
                                images_ulr_vi,comments_vi,operatormi,datami,images_ulr_mi):
        pdf_buffer = BytesIO()

        # Configurar el PDF
        c = canvas.Canvas(pdf_buffer, pagesize=letter)
        width, height = letter  # Default letter size
        margin = 70
        #print("medidas",width, height)

        # Configuración de los estilos de párrafo
        styles = getSampleStyleSheet()
        styleNormal = styles['Normal']
        styleNormal.alignment = 4
        styleNormal.fontSize = 10
        styleNormal.leading = 12 


        #CMS AND CERN IMAGE
        with_log=67.5
        height_log=67.5
        cern_image_path=os.path.join(BASE_DIR, 'static/images/logo_CERN.png')
        cern_image = pli.Image.open(cern_image_path)

        c.drawInlineImage(cern_image, margin, 750-height_log, width=with_log, height=height_log)


        # cms
        cms_image_path=os.path.join(BASE_DIR, 'static/images/logo-cms.png')#"./static/images/logo-cms.png"
        cms_image = pli.Image.open(cms_image_path)

        # Draw the image
        c.drawInlineImage(cms_image, margin+10+with_log, 750-height_log, width=with_log, height=height_log)

        yflogo=750-height_log
        #print("valores imagen",scale,scale*image_width,scale*image_height)

        c.setTitle(f"RO_QC_{boardName}")

        # Alineación a la derecha

        c.setFont("Helvetica", 12)
        

        # Obtener la fecha actual
        current_date = datetime.now()

        # Formatear la fecha al formato deseado
        formatted_date = current_date.strftime("%B %d de %Y")
        text_to_align_right = f"Geneva, {formatted_date}"
        text_width = c.stringWidth(text_to_align_right, "Helvetica", 12)
        text_x_right = width - margin - text_width

        c.drawString(text_x_right, yflogo-30, text_to_align_right)  # Adjust y-coordinate as needed

        yfecha=yflogo-30

        # Header
        c.setFont("Helvetica-Bold", 14)
        c.drawString(margin, yfecha-30, f"{gemName} QC REPORT: {boardName}")

        
        yqcreport=yfecha-30

        # Information Section
        c.setFont("Helvetica-Bold", 13)
        text = "Information:"

        # Dibujar el texto
        c.drawString(margin, yqcreport-30, text)

        # Calcular el ancho del texto para el subrayado
        text_width = c.stringWidth(text, "Helvetica-Bold", 13)
        c.line(70, yqcreport-30 - 2, margin + text_width, yqcreport-30 - 2)

        ytitleinformation=yqcreport-30 - 2

        #TEXT INFORMATION
        
        text_information = geminformation#"""Entre las noticias, informaciones y chismes de la violenta embestida digital, lo que las audiencias online más valorizan puede ser sorprendente: materias largas, cobertura profundizada e investigación periodística que ofrezca nuevas perspectivas. Esa la conclusión de un reciente estudio que evaluó un conjunto pionero de análisis de la media usado por el Instituto de Prensa Americano [American Press Institute-API] para rastrear el envolvimiento de los lectores."""

        # Crear un objeto Paragraph
        paragraph = Paragraph(text_information, styleNormal)

        text_height = paragraph.wrapOn(c, width - 2 * margin, height - 2 * margin)[1]

        # Dibujar el texto en el PDF en la posición x,y especificada
        paragraph.drawOn(c, margin, ytitleinformation -10- text_height)

        yfinformation=ytitleinformation -10- text_height

        # Visual Inspection:
        c.setFont("Helvetica-Bold", 13)
        textvi = "Visual Inspection:"

        # Dibujar el texto
        c.drawString(margin, yfinformation-30, textvi)

        # Calcular el ancho del texto para el subrayado
        text_widthvi = c.stringWidth(textvi, "Helvetica-Bold", 13)
        c.line(70, yfinformation-30 - 2, margin + text_widthvi, yfinformation-30 - 2)

        yfinformationef=yfinformation-30 - 2

        #data inspection

        c.setFont("Helvetica", 11)
        c.drawString(margin, yfinformationef-30, f"Operator(s) : {operatorvi.title()}")

        #time inspection

        c.setFont("Helvetica", 11)
        datavic=datetime.strptime(datavi, "%Y-%m-%d").strftime('%B %d de %Y')
        c.drawString(margin, yfinformationef-50, f"Date of inspection: {datavic}")




        yfinformatione=yfinformationef-60

        #IMAGE 

        styles = getSampleStyleSheet()
        style = styles["Normal"]  # Estilo para los comentario

        #margin = inch * 0.5
        column_width = (width - 3 * margin) / 2  # Dos columnas, margin entre ellas
        image_height = column_width#(height - 11 * margin) / 10  # Espacio para 10 imágenes, considerando márgenes adicionales para comentarios

        # Lista de imágenes y comentarios (asegúrate de que las rutas y los comentarios sean correctos)
        images_and_comments=[(i[1:],j) for i,j in zip(images_ulr_vi,comments_vi)]
        #print("timages_and_comments",images_and_comments)
        
        es_primera_pagina = True

        index_in=0

        #comment_width, comment_height=0,0

        
        coment_arr=[[0,0]]

        arrcomc=[]

        for index, (image_path, comment) in enumerate(images_and_comments):
            paragraph = Paragraph(comment, style)
            comment_width, comment_height = paragraph.wrap(column_width, margin) 
            arrcomc.append(comment_height)
            
            if len(images_and_comments)-1==index:
                coment_arr.append(arrcomc)
                arrcomc=[]             
            else:
                if len(arrcomc)>1:
                    coment_arr.append(arrcomc)
                    arrcomc=[]
            
        



        texthgt=0

        for index, (image_path, comment) in enumerate(images_and_comments):
            column=index%2 # 0 para la primera columna, 1 para la segunda
            row = index_in // 2  # Incrementa cada vez que se pasa a la siguiente pareja de imágenes
            rown = index // 2



            # Calcular posición de la imagen
            x = margin + (column_width + margin) * column


            #y = yfinformation  # Ajustar para que las imágenes se posicionen desde la parte superior

            # Cargar y dibujar imagen
            #print(image_path)
            img = imr.Image(image_path)

            # Obtener dimensiones originales de la imagen
            img_width, img_height = img.imageWidth, img.imageHeight

            # Calcular el factor de escala necesario para ajustar la imagen al ancho de la columna
            scale_factor = column_width / img_width

            # Ajustar las dimensiones de la imagen al factor de escala
            scaled_width = 200 #img_width * scale_factor
            scaled_height =240 #img_height * scale_factor

            # Configurar las dimensiones ajustadas en la imagen
            img.drawWidth = scaled_width
            img.drawHeight = scaled_height

            # Calcular la nueva posición ajustada de y para la imagen, considerando el margen superior

            
            if rown==0:
                y_new=yfinformatione-row*scaled_height-max(coment_arr[rown])
            else:
                y_new=yfinformatione-row*scaled_height-1.3*max(coment_arr[rown])-max(coment_arr[rown-1])


            #print("coment_arr[rown]",coment_arr[rown],"new opction",coment_arr[:rown+1])

            if y_new-scaled_height<margin:
                yfinformatione=height-margin                
                y=height-margin
                es_primera_pagina = False
                index_in=1
                c.showPage()              
                
            else:
                index_in=index_in+1
                if es_primera_pagina==False:
                    yfinformatione=height-margin 
                    y=height-margin
                    es_primera_pagina = True
                else:                    
                    y=y_new 
                    

            paragraph = Paragraph(comment, style)
            comment_width, comment_height = paragraph.wrap(column_width, margin)  # Asignar espacio para el comentario
            # Ajustar la posición del comentario para que aparezca justo debajo de la imagen
            
            
            y_adjusted = y - scaled_height
            img.drawOn(c, x, y_adjusted)

            
            paragraph.drawOn(c, x, y_adjusted- comment_height)
        
        try:
            yfinalvii=y_adjusted- comment_height
        except:
            yfinalvii=yfinformatione

        # Microscope Inspection:

        if gemName=="ME0":
            if len(images_ulr_mi)>0:
                c.setFont("Helvetica-Bold", 13)
                textmi = "Microscope Inspection:"

                # Calcular el ancho del texto para el subrayado
                text_widthvi = c.stringWidth(textmi, "Helvetica-Bold", 13)
                

                yfinmispection=yfinalvii-30 - 2

                # MO MICROSCOPI INSPECTION
                misp_image_path=os.path.join(BASE_DIR, 'static/images/BM0.png')#"./static/images/logo-cms.png"


                # Draw the image
                wmo=220

                if yfinmispection-50-wmo<margin:
                    c.showPage()
                    yfinalvii=height-margin+30
                    
                # Dibujar el texto
                c.setFont("Helvetica-Bold", 13)
                
                c.drawString(margin, yfinalvii-30, textmi)
                c.line(70, yfinalvii-30 - 2, margin + text_widthvi, yfinalvii-30 - 2)


                #data inspection

                yfinmispection=yfinalvii-30 - 2

                c.setFont("Helvetica", 11)
                c.drawString(margin, yfinmispection-30, f"Operator(s) : {operatormi.title()}")

                #time inspection

                c.setFont("Helvetica", 11)

                datamic=datetime.strptime(datami, "%Y-%m-%d").strftime('%B %d de %Y')
                c.drawString(margin, yfinmispection-50, f"Date of inspection: {datamic}")

                fdmiout=yfinmispection-50

                imgmeo = imr.Image(misp_image_path)



                # Configurar las dimensiones ajustadas en la imagen
                imgmeo.drawWidth = wmo
                imgmeo.drawHeight = wmo
            
                imgmeo.drawOn(c, width/2-wmo/2, fdmiout-20-wmo)

                yfdrabami=fdmiout-20-wmo


                column_widthmi = (width - 2* margin) / 2  # Dos columnas, margin entre ellas
                image_heightmi = column_width#(height - 11 * margin) / 10  # Espacio para 10 imágenes, considerando márgenes adicionales para comentarios
                


                titulo_style = ParagraphStyle(
                    'Titulo',
                    parent=styles['Normal'],
                    fontName='Helvetica-Bold',
                    fontSize=25,
                    leading=20,
                    spaceAfter=10,
                    alignment=1,  # Centrar el título

                )

                x0 = margin + (column_widthmi) * 0
                x1 = margin + (column_widthmi) * 1


                #SECTOR A
                #==================== B =============================
                print("A")
                filtro = ['A-b', 'A-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("A",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:
                    

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)


                #==================== B =============================
                print("B")
                filtro = ['B-b', 'B-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("B",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== C =============================
                print("C")
                filtro = ['C-b', 'C-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("C",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== D =============================
                print("D")
                filtro = ['D-b', f'D-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("D",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== E =============================
                print("E")
                filtro = ['E-b', 'E-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("E",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== F =============================
                print("F")
                filtro = ['F-b', f'F-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("F",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== G =============================
                print("G")
                filtro = ['G-b', 'G-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("G",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== H =============================
                print("H")
                filtro = ['H-b', 'H-d']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("H",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

        if gemName=="GE2/1":
            if len(images_ulr_mi)>0:
                c.setFont("Helvetica-Bold", 13)
                textmi = "Microscope Inspection:"

                # Calcular el ancho del texto para el subrayado
                text_widthvi = c.stringWidth(textmi, "Helvetica-Bold", 13)
                

                yfinmispection=yfinalvii-30 - 2

                # MO MICROSCOPI INSPECTION
                misp_image_path=os.path.join(BASE_DIR, 'static/images/GE21.png')#"./static/images/logo-cms.png"


                # Draw the image
                wmo=220

                if yfinmispection-50-wmo<margin:
                    c.showPage()
                    yfinalvii=height-margin+30
                    
                # Dibujar el texto
                c.setFont("Helvetica-Bold", 13)
                
                c.drawString(margin, yfinalvii-30, textmi)
                c.line(70, yfinalvii-30 - 2, margin + text_widthvi, yfinalvii-30 - 2)


                #data inspection

                yfinmispection=yfinalvii-30 - 2

                c.setFont("Helvetica", 11)
                c.drawString(margin, yfinmispection-30, f"Operator(s) : {operatormi.title()}")

                #time inspection

                c.setFont("Helvetica", 11)

                datamic=datetime.strptime(datami, "%Y-%m-%d").strftime('%B %d de %Y')
                c.drawString(margin, yfinmispection-50, f"Date of inspection: {datamic}")

                fdmiout=yfinmispection-50

                imgmeo = imr.Image(misp_image_path)



                # Configurar las dimensiones ajustadas en la imagen
                imgmeo.drawWidth = wmo+30
                imgmeo.drawHeight = wmo-10
            
                imgmeo.drawOn(c, width/2-wmo/2, fdmiout-20-wmo)

                yfdrabami=fdmiout-20-wmo


                column_widthmi = (width - 2* margin) / 2  # Dos columnas, margin entre ellas
                image_heightmi = column_width#(height - 11 * margin) / 10  # Espacio para 10 imágenes, considerando márgenes adicionales para comentarios
                


                titulo_style = ParagraphStyle(
                    'Titulo',
                    parent=styles['Normal'],
                    fontName='Helvetica-Bold',
                    fontSize=25,
                    leading=20,
                    spaceAfter=10,
                    alignment=1,  # Centrar el título

                )

                x0 = margin + (column_widthmi) * 0
                x1 = margin + (column_widthmi) * 1


                #SECTOR A
                #==================== B =============================
                print("1")
                filtro = ['1-B', '1-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("1",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:
                    

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)


                #==================== B =============================
                print("2")
                filtro = ['2-B', '2-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("2",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== C =============================
                print("3")
                filtro = ['3-B', '3-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("3",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== D =============================
                print("4")
                filtro = ['4-B', '4-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("4",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== E =============================
                print("5")
                filtro = ['5-B', '5-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("5",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== F =============================
                print("6")
                filtro = ['6-B', '6-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("6",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== G =============================
                print("7")
                filtro = ['7-B', '7-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("7",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)

                #==================== H =============================
                print("8")
                filtro = ['8-B', '8-D']
                    #print("filtro",filtro)

                    # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]

                paragraph = Paragraph("8",titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)

                yfdrabami=y_adjusted

                if len(imagenes_filtradas)==2:

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x0, y_adjusted)

                    #segunda imagen                

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x1, y_adjusted)








            #================================


            

            # for index, image_path in enumerate(images_ulr_mi):

            #     column=index%2 # 0 para la primera columna, 1 para la segunda
            #     rown = index // 2



            #     # Calcular posición de la imagen
            #     x = x = margin + (column_widthmi) * column


            #     #y = yfinformation  # Ajustar para que las imágenes se posicionen desde la parte superior

            #     # Cargar y dibujar imagen
            #     #print(image_path)
            #     img = imr.Image(image_path[1:])

            #     # Obtener dimensiones originales de la imagen
            #     img_width, img_height = img.imageWidth, img.imageHeight

            #     # Calcular el factor de escala necesario para ajustar la imagen al ancho de la columna
            #     scale_factor = column_width / img_width

            #     # Ajustar las dimensiones de la imagen al factor de escala
            #     scaled_width = img_width * scale_factor
            #     scaled_height = img_height * scale_factor

            #     # Configurar las dimensiones ajustadas en la imagen
            #     img.drawWidth = scaled_width
            #     img.drawHeight = scaled_height

                
                # Calcular la nueva posición ajustada de y para la imagen, considerando el margen superior

                
                # if rown==0:
                #     y_new=yfinformatione-row*scaled_height-max(coment_arr[rown])
                # else:
                #     y_new=yfinformatione-row*scaled_height-1.3*max(coment_arr[rown])-max(coment_arr[rown-1])


                # #print("coment_arr[rown]",coment_arr[rown],"new opction",coment_arr[:rown+1])

                # if y_new-scaled_height<margin:
                #     yfinformatione=height-margin                
                #     y=height-margin
                #     es_primera_pagina = False
                #     index_in=1
                #     c.showPage()              
                    
                # else:
                #     index_in=index_in+1
                #     if es_primera_pagina==False:
                #         yfinformatione=height-margin 
                #         y=height-margin
                #         es_primera_pagina = True
                #     else:                    
                #         y=y_new 
                        

                # paragraph = Paragraph(comment, style)
                # comment_width, comment_height = paragraph.wrap(column_width, margin)  # Asignar espacio para el comentario
                # # Ajustar la posición del comentario para que aparezca justo debajo de la imagen
                
                
                # y_adjusted = y - scaled_height
                # img.drawOn(c, x, y_adjusted)

                
                # paragraph.drawOn(c, x, y_adjusted- comment_height)

        
        """
        
        
        if len(images_ulr_mi)>0:
            c.setFont("Helvetica-Bold", 13)
            textmi = "Microscope Inspection:"

            # Calcular el ancho del texto para el subrayado
            text_widthvi = c.stringWidth(textmi, "Helvetica-Bold", 13)
            

            yfinmispection=yfinalvii-30 - 2

            # MO MICROSCOPI INSPECTION
            misp_image_path=os.path.join(BASE_DIR, 'static/images/BM0.png')#"./static/images/logo-cms.png"


            # Draw the image
            wmo=220

            if yfinmispection-50-wmo<margin:
                c.showPage()
                yfinalvii=height-margin+30
                
            # Dibujar el texto
            c.setFont("Helvetica-Bold", 13)
            
            c.drawString(margin, yfinalvii-30, textmi)
            c.line(70, yfinalvii-30 - 2, margin + text_widthvi, yfinalvii-30 - 2)


            #data inspection

            yfinmispection=yfinalvii-30 - 2

            c.setFont("Helvetica", 11)
            c.drawString(margin, yfinmispection-30, f"Operator(s) : {operatormi.title()}")

            #time inspection

            c.setFont("Helvetica", 11)

            datamic=datetime.strptime(datami, "%Y-%m-%d").strftime('%B %d de %Y')
            c.drawString(margin, yfinmispection-50, f"Date of inspection: {datamic}")

            fdmiout=yfinmispection-50

            imgmeo = imr.Image(misp_image_path)



            # Configurar las dimensiones ajustadas en la imagen
            imgmeo.drawWidth = wmo
            imgmeo.drawHeight = wmo
        
            imgmeo.drawOn(c, width/2-wmo/2, fdmiout-20-wmo)

            yfdrabami=fdmiout-20-wmo


            column_widthmi = (width - 2* margin) / 2  # Dos columnas, margin entre ellas
            image_heightmi = column_width#(height - 11 * margin) / 10  # Espacio para 10 imágenes, considerando márgenes adicionales para comentarios


            mo=["A","B","C","D","E","F","G","H"]

            titulo_style = ParagraphStyle(
                'Titulo',
                parent=styles['Normal'],
                fontName='Helvetica-Bold',
                fontSize=25,
                leading=20,
                spaceAfter=10,
                alignment=1,  # Centrar el título

            )
            

            for i in mo:
    
                paragraph = Paragraph(i,titulo_style)
                comment_width, comment_height = paragraph.wrap(column_width, margin)
                

                y_adjusted = yfdrabami - 0.6*column_widthmi-comment_height-5
                #print("y_adjusted",y_adjusted)

                if y_adjusted<margin:
                    y_adjusted=height-margin- 0.6*column_widthmi -comment_height-5             
                    yfdrabami=height-margin

                    c.showPage() 
                
                

                paragraph.drawOn(c, margin, yfdrabami-comment_height)
                yfdrabami=y_adjusted

                filtro = [f'{i}-b', f'{i}-d']
                #print("filtro",filtro)

                # Filtrar imágenes
                imagenes_filtradas = [img[1:] for img in images_ulr_mi if any(f in img for f in filtro)]
                print("imagenes_filtradas",imagenes_filtradas)

                if len(imagenes_filtradas)==2: 
                    x = margin + (column_widthmi) * 0

                    img = imr.Image(imagenes_filtradas[0])

                    # Obtener dimensiones originales de la imagen

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    
                    
                    img.drawOn(c, x, y_adjusted)

                    #segunda imagen

                    x = margin + (column_widthmi) * 1

                    img = imr.Image(imagenes_filtradas[1])

                    # Configurar las dimensiones ajustadas en la imagen
                    img.drawWidth = column_widthmi
                    img.drawHeight = 0.6*column_widthmi

                    img.drawOn(c, x, y_adjusted)
                else:

                    pass
        
        
        










        #c.drawInlineImage(misp_image, margin, fdmiout-20-wmo, width=wmo, height=wmo)





        
        #print(coment_arr)  """





        """
        # Header
        c.setFont("Helvetica-Bold", 14)
        c.drawString(72, 750, "GE2/1 QC REPORT: GE21-RO-M6-0017")

        # Information Section
        c.setFont("Helvetica", 12)
        c.drawString(72, 720, "Information:")
        c.setFont("Helvetica", 10)
        c.drawString(72, 705, "Part of PCB batch C (sub-batch C1) delivered at 904 on Sep. 26 2023")
        c.drawString(72, 690, "Geneva date 2023")

        # Visual Inspection
        c.setFont("Helvetica-Bold", 12)
        c.drawString(72, 660, "Visual Inspection:")
        c.setFont("Helvetica", 10)
        c.drawString(72, 645, "Operator(s) : Aya Beshr")
        c.drawString(72, 630, "Date of inspection: Sep 27 2023")
        c.drawString(72, 615, "1 Minor defect: residue of adhesive material can be removed with ethanol cleaning")

        # Microscope Inspection
        c.setFont("Helvetica-Bold", 12)
        c.drawString(72, 585, "Microscope Inspection")
        c.setFont("Helvetica", 10)
        c.drawString(72, 570, "Operator(s) : Jeremie Merlin")
        c.drawString(72, 555, "Date of inspection: Sep 28 2023")"""

        c.showPage()
        c.save()

        # Mover el puntero del buffer al inicio para que esté listo para leer
        pdf_buffer.seek(0)

        return pdf_buffer
    
    def clean(self,request):
        self.context = {'image_urlsvi': [], 'image_urlsmi': []}
        return render(request, 'home.html',self.context)
